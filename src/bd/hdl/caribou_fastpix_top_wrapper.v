//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
//Date        : Thu Oct  6 13:55:00 2022
//Host        : Alphys running 64-bit unknown
//Command     : generate_target caribou_fastpix_top_wrapper.bd
//Design      : caribou_fastpix_top_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module caribou_fastpix_top_wrapper
   (DDR_addr,
    DDR_ba,
    DDR_cas_n,
    DDR_ck_n,
    DDR_ck_p,
    DDR_cke,
    DDR_cs_n,
    DDR_dm,
    DDR_dq,
    DDR_dqs_n,
    DDR_dqs_p,
    DDR_odt,
    DDR_ras_n,
    DDR_reset_n,
    DDR_we_n,
    FIXED_IO_ddr_vrn,
    FIXED_IO_ddr_vrp,
    FIXED_IO_mio,
    FIXED_IO_ps_clk,
    FIXED_IO_ps_porb,
    FIXED_IO_ps_srstb,
    FMC_LPC_LA02_N,
    FMC_LPC_LA02_P,
    FMC_LPC_LA07_N,
    FMC_LPC_LA07_P,
    FMC_LPC_LA21_N,
    FMC_LPC_LA21_P,
    FMC_LPC_LA30_N,
    FMC_LPC_LA30_P,
    FMC_REF_CLK_N,
    FMC_REF_CLK_P,
    TLU_CLK_N,
    TLU_CLK_P,
    TLU_T0_N,
    TLU_T0_P,
    fmc_clk_n,
    fmc_clk_p,
    gpio_n,
    gpio_p,
    sys_diff_clock_clk_n,
    sys_diff_clock_clk_p,
    user_sma_clk_n,
    user_sma_clk_p);
  inout [14:0]DDR_addr;
  inout [2:0]DDR_ba;
  inout DDR_cas_n;
  inout DDR_ck_n;
  inout DDR_ck_p;
  inout DDR_cke;
  inout DDR_cs_n;
  inout [3:0]DDR_dm;
  inout [31:0]DDR_dq;
  inout [3:0]DDR_dqs_n;
  inout [3:0]DDR_dqs_p;
  inout DDR_odt;
  inout DDR_ras_n;
  inout DDR_reset_n;
  inout DDR_we_n;
  inout FIXED_IO_ddr_vrn;
  inout FIXED_IO_ddr_vrp;
  inout [53:0]FIXED_IO_mio;
  inout FIXED_IO_ps_clk;
  inout FIXED_IO_ps_porb;
  inout FIXED_IO_ps_srstb;
  input FMC_LPC_LA02_N;
  input FMC_LPC_LA02_P;
  input FMC_LPC_LA07_N;
  input FMC_LPC_LA07_P;
  input FMC_LPC_LA21_N;
  input FMC_LPC_LA21_P;
  output FMC_LPC_LA30_N;
  output FMC_LPC_LA30_P;
  output FMC_REF_CLK_N;
  output FMC_REF_CLK_P;
  input TLU_CLK_N;
  input TLU_CLK_P;
  input TLU_T0_N;
  input TLU_T0_P;
  input fmc_clk_n;
  input fmc_clk_p;
  output [7:0]gpio_n;
  output [7:0]gpio_p;
  input sys_diff_clock_clk_n;
  input sys_diff_clock_clk_p;
  output user_sma_clk_n;
  output user_sma_clk_p;

  wire [14:0]DDR_addr;
  wire [2:0]DDR_ba;
  wire DDR_cas_n;
  wire DDR_ck_n;
  wire DDR_ck_p;
  wire DDR_cke;
  wire DDR_cs_n;
  wire [3:0]DDR_dm;
  wire [31:0]DDR_dq;
  wire [3:0]DDR_dqs_n;
  wire [3:0]DDR_dqs_p;
  wire DDR_odt;
  wire DDR_ras_n;
  wire DDR_reset_n;
  wire DDR_we_n;
  wire FIXED_IO_ddr_vrn;
  wire FIXED_IO_ddr_vrp;
  wire [53:0]FIXED_IO_mio;
  wire FIXED_IO_ps_clk;
  wire FIXED_IO_ps_porb;
  wire FIXED_IO_ps_srstb;
  wire FMC_LPC_LA02_N;
  wire FMC_LPC_LA02_P;
  wire FMC_LPC_LA07_N;
  wire FMC_LPC_LA07_P;
  wire FMC_LPC_LA21_N;
  wire FMC_LPC_LA21_P;
  wire FMC_LPC_LA30_N;
  wire FMC_LPC_LA30_P;
  wire FMC_REF_CLK_N;
  wire FMC_REF_CLK_P;
  wire TLU_CLK_N;
  wire TLU_CLK_P;
  wire TLU_T0_N;
  wire TLU_T0_P;
  wire fmc_clk_n;
  wire fmc_clk_p;
  wire [7:0]gpio_n;
  wire [7:0]gpio_p;
  wire sys_diff_clock_clk_n;
  wire sys_diff_clock_clk_p;
  wire user_sma_clk_n;
  wire user_sma_clk_p;

  caribou_fastpix_top caribou_fastpix_top_i
       (.DDR_addr(DDR_addr),
        .DDR_ba(DDR_ba),
        .DDR_cas_n(DDR_cas_n),
        .DDR_ck_n(DDR_ck_n),
        .DDR_ck_p(DDR_ck_p),
        .DDR_cke(DDR_cke),
        .DDR_cs_n(DDR_cs_n),
        .DDR_dm(DDR_dm),
        .DDR_dq(DDR_dq),
        .DDR_dqs_n(DDR_dqs_n),
        .DDR_dqs_p(DDR_dqs_p),
        .DDR_odt(DDR_odt),
        .DDR_ras_n(DDR_ras_n),
        .DDR_reset_n(DDR_reset_n),
        .DDR_we_n(DDR_we_n),
        .FIXED_IO_ddr_vrn(FIXED_IO_ddr_vrn),
        .FIXED_IO_ddr_vrp(FIXED_IO_ddr_vrp),
        .FIXED_IO_mio(FIXED_IO_mio),
        .FIXED_IO_ps_clk(FIXED_IO_ps_clk),
        .FIXED_IO_ps_porb(FIXED_IO_ps_porb),
        .FIXED_IO_ps_srstb(FIXED_IO_ps_srstb),
        .FMC_LPC_LA02_N(FMC_LPC_LA02_N),
        .FMC_LPC_LA02_P(FMC_LPC_LA02_P),
        .FMC_LPC_LA07_N(FMC_LPC_LA07_N),
        .FMC_LPC_LA07_P(FMC_LPC_LA07_P),
        .FMC_LPC_LA21_N(FMC_LPC_LA21_N),
        .FMC_LPC_LA21_P(FMC_LPC_LA21_P),
        .FMC_LPC_LA30_N(FMC_LPC_LA30_N),
        .FMC_LPC_LA30_P(FMC_LPC_LA30_P),
        .FMC_REF_CLK_N(FMC_REF_CLK_N),
        .FMC_REF_CLK_P(FMC_REF_CLK_P),
        .TLU_CLK_N(TLU_CLK_N),
        .TLU_CLK_P(TLU_CLK_P),
        .TLU_T0_N(TLU_T0_N),
        .TLU_T0_P(TLU_T0_P),
        .fmc_clk_n(fmc_clk_n),
        .fmc_clk_p(fmc_clk_p),
        .gpio_n(gpio_n),
        .gpio_p(gpio_p),
        .sys_diff_clock_clk_n(sys_diff_clock_clk_n),
        .sys_diff_clock_clk_p(sys_diff_clock_clk_p),
        .user_sma_clk_n(user_sma_clk_n),
        .user_sma_clk_p(user_sma_clk_p));
endmodule

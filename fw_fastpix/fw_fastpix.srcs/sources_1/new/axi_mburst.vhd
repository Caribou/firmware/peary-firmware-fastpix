----------------------------------------------------------------------------------
-- Company: Brookhaven National Laboratory
-- Engineer: Elena Zhivun <ezhivun@bnl.gov>
-- 
-- Create Date:    11:38:56 02/06/2012 
-- Design Name:   axi_mburst
-- Module Name:    axi_minterface - Behavioral 
-- Project Name:  CaRIBOu AD9249 interface driver
-- Target Devices: Zynq- ZC706
-- Tool versions: Vivado 2018.1
-- Description:
--
-- The module retreives ADC data from FIFO and send it to CPU in bursts.
-- FIFO data is pre-fetched to maximize the transfer rate
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
-- library UNISIM;
--use UNISIM.VComponents.all;

entity axi_mburst is
  Generic(
    start_addr : std_logic_vector(27 downto 0) := X"000_0000";
    end_addr   : std_logic_vector(27 downto 0) := X"3FF_FFFF";
    BURST_LEN  : integer := 256 --  1 ... 256
  );
  port (
    axi_clk   : in std_logic;
    axi_reset : in std_logic; -- active low synchronous reset
    addr_rst  : in std_logic;

    fifo_reset : in  std_logic;
    fifo_wrclk : in  std_logic;
    fifo_data  : in  std_logic_vector(127 downto 0);
    fifo_wren  : in  std_logic;
    fifo_wrcnt : out std_logic_vector(9 downto 0);
    burst_addr : out std_logic_vector(31 downto 0);
    axi_fifo_full : out std_logic;

    axi_awready : in  std_logic;
    axi_awaddr  : out std_logic_vector(31 downto 0); --32bit-->28bit: top.vhd assign high 4 bit
    axi_awvalid : out std_logic;
    axi_awlen   : out std_logic_vector(7 downto 0);
    axi_awsize  : out std_logic_vector(2 downto 0);
    axi_awburst : out std_logic_vector(1 downto 0);
    axi_awcache : out std_logic_vector(3 downto 0);
    axi_awprot  : out std_logic_vector(2 downto 0);

    axi_wready : in  std_logic;
    axi_wdata  : out std_logic_vector(63 downto 0);
    axi_wvalid : out std_logic;
    axi_wstrb  : out std_logic_vector(7 downto 0);
    axi_wlast  : out std_logic;

    axi_bvalid : in  std_logic;
    axi_bresp  : in  std_logic_vector(1 downto 0);
    axi_bready : out std_logic
  );
end axi_mburst;

architecture Behavioral of axi_mburst is

  component adc_fifo
    port (
      rst         : IN  STD_LOGIC;
      wr_clk      : IN  STD_LOGIC;
      rd_clk      : IN  STD_LOGIC;
      din         : IN  STD_LOGIC_VECTOR(127 DOWNTO 0);
      wr_en       : IN  STD_LOGIC;
      rd_en       : IN  STD_LOGIC;
      dout        : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      wr_data_count : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
      full        : OUT STD_LOGIC;
      empty       : OUT STD_LOGIC;
      prog_empty   : OUT STD_LOGIC;
      wr_rst_busy : OUT STD_LOGIC;
      rd_rst_busy : OUT STD_LOGIC
    );
  END COMPONENT;

  type state_type is (s_init, s_idle, s_send_addr, s_wait_awready, s_send_one,
      s_send, s_wait_write_response, s_incr_write_addr, s_reset_write_addr,
      s_wait_reset_ack, s_wait_wready, s_pre_fetch);
  signal state: state_type;

  constant BLOCK_SIZE : INTEGER := BURST_LEN * 8; --256Bursts*8Bytes

  signal burstcnt : INTEGER RANGE 0 TO BURST_LEN-1;

  signal axi_awaddr_i : std_logic_vector(27 downto 0) := start_addr;

  signal fifo_rden        : std_logic;
  signal fifo_rddata      : std_logic_vector(63 downto 0);
  signal fifo_rddata_prev  : std_logic_vector(63 downto 0) := (others => '0');
  signal fifo_full      : std_logic;
  signal fifo_prog_empty : std_logic;
  signal fifo_empty     : std_logic;
  signal axi_wvalid_s   : std_logic;
  signal axi_wready_prev   : std_logic;

  signal addr_rst_req : std_logic := '0';
  signal addr_rst_ack : std_logic := '0';

  -- ILA probe readback signals
  signal axi_awaddr_s  : std_logic_vector(axi_awaddr'range); --32bit-->28bit: top.vhd assign high 4 bit
  signal burst_addr_s : std_logic_vector(burst_addr'range);
  signal axi_awvalid_s : std_logic;
  signal axi_awlen_s   : std_logic_vector(axi_awlen'range);
  signal axi_awsize_s  : std_logic_vector(axi_awsize'range);
  signal axi_awburst_s : std_logic_vector(axi_awburst'range);
  signal axi_wdata_s  : std_logic_vector(axi_wdata'range);
  signal axi_wstrb_s  : std_logic_vector(axi_wstrb'range);
  signal axi_wlast_s  : std_logic;
  signal axi_bready_s   : std_logic;

-----------------------------------------------------------------------------------
-- Debug signals

attribute mark_debug : string;
attribute dont_touch : string;

attribute mark_debug of axi_reset : signal is "true";
attribute mark_debug of addr_rst : signal is "true";
attribute mark_debug of axi_awready : signal is "true";
attribute mark_debug of axi_awaddr_s : signal is "true";
attribute mark_debug of axi_awvalid_s : signal is "true";
attribute mark_debug of axi_awlen_s : signal is "true";
attribute mark_debug of axi_awsize_s : signal is "true";
attribute mark_debug of axi_awburst_s : signal is "true";
attribute mark_debug of axi_wready : signal is "true";
attribute mark_debug of axi_wdata_s : signal is "true";
attribute mark_debug of axi_wvalid_s : signal is "true";
attribute mark_debug of axi_wstrb_s : signal is "true";
attribute mark_debug of axi_wlast_s : signal is "true";
attribute mark_debug of axi_bvalid : signal is "true";
attribute mark_debug of axi_bresp : signal is "true";
attribute mark_debug of axi_bready_s : signal is "true";
attribute mark_debug of burst_addr_s : signal is "true";
  

attribute dont_touch of axi_reset : signal is "true";
attribute dont_touch of addr_rst : signal is "true";
attribute dont_touch of axi_awready : signal is "true";
attribute dont_touch of axi_awaddr_s : signal is "true";
attribute dont_touch of axi_awvalid_s : signal is "true";
attribute dont_touch of axi_awlen_s : signal is "true";
attribute dont_touch of axi_awsize_s : signal is "true";
attribute dont_touch of axi_awburst_s : signal is "true";
attribute dont_touch of axi_wready : signal is "true";
attribute dont_touch of axi_wdata_s : signal is "true";
attribute dont_touch of axi_wvalid_s : signal is "true";
attribute dont_touch of axi_wstrb_s : signal is "true";
attribute dont_touch of axi_wlast_s : signal is "true";
attribute dont_touch of axi_bvalid : signal is "true";
attribute dont_touch of axi_bresp : signal is "true";
attribute dont_touch of axi_bready_s : signal is "true";
attribute dont_touch of burst_addr_s : signal is "true";
-----------------------------------------------------------------------------------

begin


  axi_wvalid <= axi_wvalid_s;  

  ----Base address is 0x10000000
  --axi_awaddr <= "0001" & axi_awaddr_i;
  axi_awaddr_s <= "0000" & axi_awaddr_i;
  burst_addr_s <= "0000" & axi_awaddr_i;

  axi_fifo_full <= fifo_full;

  -- ILA probe readback signals
  burst_addr <= burst_addr_s;
  axi_awaddr <= axi_awaddr_s;
  axi_awvalid <= axi_awvalid_s;
  axi_awlen <= axi_awlen_s;
  axi_awsize <= axi_awsize_s;
  axi_awburst <= axi_awburst_s;
  axi_wdata <= axi_wdata_s;
  axi_wstrb <= axi_wstrb_s;
  axi_wlast <= axi_wlast_s;
  axi_bready <= axi_bready_s;


  adc0_fifo : adc_fifo
    port map(
      rst           => fifo_reset,
      wr_clk        => fifo_wrclk,
      rd_clk        => axi_clk,
      din           => fifo_data,
      wr_en         => fifo_wren,
      rd_en         => fifo_rden,
      dout          => fifo_rddata,
      full          => fifo_full,
      wr_data_count => fifo_wrcnt,
      prog_empty     => fifo_prog_empty,
      empty          => fifo_empty
    );


-- synthesis translate_off
  verify_fifo_read : process(axi_clk) is
  begin
    if rising_edge(axi_clk) then
      if fifo_rden = '1' then
        assert fifo_empty = '0' report "Reading from empty FIFO" severity FAILURE;
      end if;
    end if;
  end process;

  verify_fifo_write : process(fifo_wrclk) is
  begin
    if rising_edge(fifo_wrclk) then
      if fifo_wren = '1' then
        assert fifo_full = '0' report "Writing to full FIFO" severity FAILURE;
      end if;
    end if;
  end process;
-- synthesis translate_on

  address_reset_handshake : process(axi_clk) is
  begin
    if rising_edge(axi_clk) then
      if addr_rst = '1' then
        addr_rst_req <= '1';
      end if;
      if addr_rst_ack = '1' then
        addr_rst_req <= '0';
      end if;
    end if;
  end process;  

  fsm_state_update : process(axi_clk) is
  begin
    if rising_edge(axi_clk) then
      if axi_reset = '0' then -- active low reset
        state <= s_init;
      else
        case (state) is
          when s_init =>
            state <= s_idle;

          when s_idle =>
            if addr_rst_req = '1' then
              state <= s_reset_write_addr;
            elsif fifo_prog_empty = '0' then
              state <= s_send_addr;
            end if;

          when s_send_addr =>
            state <= s_wait_awready;

          when s_wait_awready =>
            if axi_awready = '1' then
              if BURST_LEN = 1 then              
                state <= s_send_one;
              else    
                state <= s_pre_fetch;
              end if;
            end if;

          when s_pre_fetch => 
            state <= s_send;

          when s_send =>
            if axi_wready = '1' and axi_wvalid_s = '1' then
              if burstcnt = BURST_LEN-2 then
                state <= s_wait_wready;
              else
                state <= s_send;
              end if;
            else
              if burstcnt = BURST_LEN-1 then
                state <= s_wait_wready;
              else
                state <= s_send;
              end if;
            end if;

          when s_send_one =>            
            if axi_wready = '1' then
              state <= s_wait_wready;
            end if;

          when s_wait_wready =>
            if axi_wready = '1' then
              state <= s_wait_write_response;
            end if;

          when s_wait_write_response =>
            if axi_bvalid = '1' then
              state <= s_incr_write_addr;
            end if;

          when s_incr_write_addr =>
            state <= s_idle;

          when s_reset_write_addr =>
            state <= s_wait_reset_ack;

          when s_wait_reset_ack =>
            if addr_rst_req = '0' then
              state <= s_idle;
            end if;

          when others =>
            null;
        end case;
      end if;
    end if;
  end process;

  axi_wready_delay : process(axi_clk) is
  begin
    if rising_edge(axi_clk) then
      axi_wready_prev <= axi_wready;
    end if;
  end process;

  fsm_variable_update : process(axi_clk) is
    variable axi_awaddr_int : integer;
    variable end_addr_int : integer;
  begin
    if rising_edge(axi_clk) then
      case (state) is
        when s_init =>
          axi_awvalid_s <= '0';
          axi_awburst_s <= "00";
          axi_awcache <= "0000";
          axi_awlen_s   <= x"00";
          axi_awprot  <= "000";
          axi_awsize_s  <= "000";
          fifo_rden   <= '0';
          axi_wvalid_s  <= '0';
          axi_wlast_s   <= '0';
          axi_wstrb_s   <= x"00";
          axi_bready_s  <= '0';
          burstcnt    <= 0;
          addr_rst_ack <= '0';
          axi_wdata_s <= (others => '0');

        when s_idle =>
          axi_wlast_s   <= '0';
          axi_bready_s  <= '0';
          axi_wvalid_s  <= '0';
          fifo_rden   <= '0';
          addr_rst_ack <= '0';

        when s_send_addr =>
          axi_awvalid_s <= '1';
          axi_awlen_s   <= std_logic_vector(to_unsigned(BURST_LEN-1, axi_awlen'length));
          axi_awburst_s <= "01";  -- incrementing address type
          axi_awsize_s  <= "011"; -- 8 bytes per beat         
          burstcnt    <= 0;
          axi_wstrb_s   <= x"FF"; -- all bytes are valid

        when s_wait_awready =>
          axi_awvalid_s <= not axi_awready;          
          if axi_awready = '1' then
            fifo_rddata_prev  <= fifo_rddata;
            fifo_rden <= '1';
            axi_wvalid_s  <= '0';
            if BURST_LEN = 1 then
              axi_wlast_s <= '1';
            else
              axi_wlast_s <= '0';
            end if;
          end if;

        when s_pre_fetch => 
          axi_wlast_s   <= '0';
          axi_bready_s  <= '0';
          axi_wvalid_s  <= '1';
          axi_wdata_s <= fifo_rddata_prev;
          fifo_rddata_prev  <= fifo_rddata;
          fifo_rden <= '1';
          
        when s_send =>
          axi_wlast_s   <= '0';
          axi_bready_s  <= '0';
          axi_wvalid_s  <= '1';

          if fifo_rden = '1' then
            fifo_rddata_prev <= fifo_rddata;
          end if;

          if axi_wready = '1' and axi_wvalid_s = '1' then
            burstcnt <= burstcnt + 1;                        

            if fifo_rden = '1' then
              axi_wdata_s <= fifo_rddata;
            else
              axi_wdata_s <= fifo_rddata_prev;
            end if;

            if burstcnt = BURST_LEN-2 then
              axi_wlast_s <= '1';
              fifo_rden <= '0';
            else
              axi_wlast_s <= '0';
              fifo_rden <= '1';              
            end if;                        
          else  
            fifo_rden <= '0';
          end if;

        when s_send_one =>
          axi_wlast_s   <= '0';
          axi_bready_s  <= '0';
          axi_wvalid_s  <= '1';
          axi_wdata_s <= fifo_rddata_prev;
          axi_wlast_s <= '1';
          fifo_rden <= '0';

        when s_wait_wready =>
          axi_wvalid_s <= not axi_wready;
          axi_wlast_s <= not axi_wready;
          fifo_rden  <= '0';
          axi_bready_s <= axi_wready;
          if axi_wready = '1' then
            burstcnt <= 0;
          end if;

        when s_wait_write_response =>          
          fifo_rden  <= '0';
          axi_wlast_s  <= '0';
          axi_wvalid_s <= '0';
          axi_bready_s <= not axi_bvalid;

        when s_incr_write_addr =>
          axi_awaddr_int := to_integer(unsigned(axi_awaddr_i));
          end_addr_int := to_integer(unsigned(end_addr));
          if (axi_awaddr_int + BLOCK_SIZE < end_addr_int) then
            axi_awaddr_i <= std_logic_vector(to_unsigned(axi_awaddr_int + BLOCK_SIZE, axi_awaddr_i'length));
          else
            axi_awaddr_i <= start_addr;
          end if;

        when s_reset_write_addr =>
          axi_awaddr_i <= start_addr;
          addr_rst_ack <= '1';

        when s_wait_reset_ack =>
          addr_rst_ack <= addr_rst_req;

        when others =>
          null;
      end case;
    end if;
  end process;

end Behavioral;

(00) timeout_pattern_conf : RW : timeout_pattern_conf[31:0]
(01) rdstatus             : R  : {24'b0, readout_busy_Saxi, chip_shutter_i3, readout_active_Saxi, chip_clk_off, chip_clk_miss_reg, chip_clk_fall_reg, chip_clk_rise_reg, 1'b0};
(02) rdcontrol            :  W : {'bU, dummy_readout_start_a, rd_shutterstart, readout_reset, readout_start_a}
(03) chipcontrol          : RW : {26'b0, chip_rst, chip_shutter, chip_tpulse, chip_pwren, 2'b0};
(04) shuttertimeout       : RW : shuttertimeout
(05) wgcontrol            :  W : {'bU, conf_write_pattern_in, gen_rst, conf_run_terminate, conf_run_start}
(06) wgstatus             : R  : {31'b0, stat_gen_running}
(07) wgcapacity           : R  : remaining_pattern_capacity
(08) wgconfruns           : RW : conf_runs_n
(09) wgpatterntime        : RW : timer_pattern_conf
(10) wgpatternoutput      : RW : {24'b0, output_pattern_conf}
(11) wgpatterntriggers    : RW : {23'b0, triggers_pattern_conf}
(12) fifo_data_read_lsb   : R  : fifo_data_read[31:0]
(13) fifo_data_read_msb   : R  : fifo_data_read[63:32]
(14) fifostatus           : R  : {31'b0, ts_fifo_data_valid}
(15) tscontrol            :  W : {30'b0, capture_enable, ts_enable}
(16) tsedgeconf           : RW : ts_signal_edge_conf[31:0]
(17) tsinittime_lsb       : RW : ts_init_value[31:0]
(18) tsinittime_msb       : RW : {16'b0, ts_init_value[47:32]}
(19) chipsignal_enable    : RW : {6'b0, chip_rst_enable, 5'b0, chip_shutter_enable, 5'b0, chip_tpulse_enable, 6'b0, chip_pwren_enable}
(20) pulser_periods       : RW : pulser_periods
(21) pulser_time_high     : RW : pulser_time_high
(22) pulser_time_low      : RW : pulser_time_low
(23) pulser_control       :  W : {30'b0, pulser_stop, pulser_start}

********************************
(00) timeout_pattern_conf
 RW
********************************
--------------------------------
[31:0] timeout_pattern_conf
--------------------------------
Pattern line parameter. Number of pattern generator clock cycles for which the generator will wait for a trigger. After elapsing the time the generator proceeds to the pattern even without satisfying a trigger condition.
0 == timeout disabled - wait forever.
It does not have an effect if trigger condition is not set for the pattern line


********************************
(01) rdstatus  
 R 
********************************
--------------------------------
[0] 1'b0
--------------------------------

--------------------------------
[1] chip_clk_rise_reg
--------------------------------
Resets to '0' on AXI_reset
Clock watchdog
Set to '1' each time a rising egde of 40MHz readout clock is registered.
Cleared when rdstatus register is read.

--------------------------------
[2] chip_clk_fall_reg
--------------------------------
Resets to '0' on AXI_reset
Clock watchdog
Set to '1' each time a falling egde of 40MHz readout clock is registered.
Cleared when rdstatus register is read.

--------------------------------
[3] chip_clk_miss_reg
--------------------------------
Resets to '0' on AXI_reset
Clock watchdog
Set to '1' each time when chip_clk_off == '1'.
Cleared when rdstatus register is read.

--------------------------------
[4] chip_clk_off
--------------------------------
Resets to '0' on AXI_reset || chip_reset
Clock watchdog
Is '1' when a 40MHz readout clock transition was missing
Is '0' when 40MHz readout clock is running.

--------------------------------
[5] readout_active_Saxi
--------------------------------
Is '1' when readout state machine is active and readout is in process
Is '0' otherwise

--------------------------------
[6] chip_shutter_i3 
--------------------------------
Output of the shutter timeout module. Shutter generated by setting a register or from a pattern generator does not appear here.

--------------------------------
[7] readout_busy_Saxi
--------------------------------
Resets to '0' on AXI_reset || chip_clk_off || readout_reset
Is set to '1' when readout fifo becomes full.
Is cleared when readout fifo is empty.

--------------------------------
[31:8] 24'b0, 
--------------------------------


Data FIFO output. Each read of the register pops next data from FIFO.
